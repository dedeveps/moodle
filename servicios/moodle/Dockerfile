#Base template
FROM ubuntu:jammy

#Server config

MAINTAINER DEDEV <dedev@profesor.usac.edu.gt>

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=America/Guatemala

RUN apt-get update && apt-get install -y \ 
	nano \ 
	net-tools \ 
	wget \  
	iproute2 \ 
	iputils-ping \ 
	software-properties-common \ 
	apt-utils \ 
	locales \ 
	tzdata \ 
	zip \ 
	unzip \ 
	sudo \ 
	netcat \ 
	&& [ -f "/etc/localtime" ] && unlink /etc/localtime \ 
	&& ln -s /usr/share/zoneinfo/America/Guatemala /etc/localtime
	
#PHP environment

RUN add-apt-repository ppa:ondrej/php -y \ 
	&& apt-get update \ 
	&& apt-get install -y apache2 \ 
	php8.1-fpm \ 
	libapache2-mod-fcgid \ 
	&& a2enconf php8.1-fpm \ 
	&& a2enmod proxy \ 
	&& a2enmod rewrite \ 
	&& a2enmod remoteip \ 
	&& a2enmod proxy_fcgi \ 
	&& apt-get install -y php8.1-curl \ 
	php8.1-zip  \  
	php8.1-gd  \  
	php8.1-intl  \  
	php8.1-xmlrpc  \  
	php8.1-soap  \  
	php8.1-imagick  \  
	php8.1-xml  \  
	php8.1-mbstring  \  
	php8.1-fpm  \  
	php8.1-mysql  \  
	php8.1-redis \ 
	imagemagick  \  
	php-imagick  \  
	php8.1-imagick  \  
	ghostscript  \  
	libgs-dev  \  
	php8.1-xml  \  
	php8.1-mbstring  \   
	cron  \  
	supervisor

COPY apache2.conf /etc/apache2/apache2.conf
COPY ports.conf /etc/apache2/ports.conf
COPY mpm_event.conf /etc/apache2/mods-available/mpm_event.conf
COPY www.conf /etc/php/8.1/fpm/pool.d/www.conf
COPY supervisor.conf /etc/supervisor.conf
COPY php.ini /etc/php/8.1/fpm/php.ini
COPY remoteip.conf /etc/apache2/conf-available/remoteip.conf

RUN a2enconf remoteip

#Moodle environment

#Fechas de Moodle en espanol
RUN sed -i -e 's/# es_ES.UTF-8 UTF-8/es_ES.UTF-8 UTF-8/' /etc/locale.gen && \ 
  dpkg-reconfigure --frontend=noninteractive locales && \ 
  update-locale LANG=es_ES.UTF-8 LC_ALL=es_ES.UTF-8 LANGUAGE=es_ES.UTF-8

RUN mkdir /var/www/moodledata && chmod 777 /var/www/moodledata && chown www-data:www-data /var/www/moodledata

#Ending

VOLUME /etc/apache2/sites-available
VOLUME /var/www/html
VOLUME /var/www/moodledata
VOLUME /var/spool/cron/crontabs

EXPOSE 80

CMD ["supervisord", "-c", "/etc/supervisor.conf"]